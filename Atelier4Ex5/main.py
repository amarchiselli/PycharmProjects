import time
import random
import matplotlib.pyplot as plt
import numpy as np

def mix_list(liste:list)->list:
    liste_mixe = []
    liste_modifie = liste
    for i in range(len(liste)):
        nb_random = random.choice(liste_modifie)
        liste_mixe.append(nb_random)
        liste_modifie.remove(nb_random)
    return liste_mixe

def test_temps(fonction:callable):
    liste_len = [10,100,1000]
    liste_elapsed = []
    for i in liste_len:
        temps = 0
        liste = [i for i in range(i)]
        for j in range(100):
            start_pc = time.perf_counter()
            fonction(liste)
            end_pc = time.perf_counter()

            temps += end_pc - start_pc
        liste_elapsed.append(temps / 100)
        
    return liste_elapsed

test_1 = test_temps(mix_list)
test_2 = test_temps(random.shuffle)

fig, ax = plt.subplots()
ax.plot([10,100,1000],test_1,'bo-',label='Identité')
ax.plot([10,100,1000],test_2, 'r*-', label='Carré')
ax.set(xlabel='Abscisse x', ylabel='Ordonnée y',title='Fonctions identité, cube et carré')
ax.legend(loc='upper center', shadow=True, fontsize='x-large')
plt.show()