from copy import copy
import time
import copy
import matplotlib.pyplot as plt
import numpy as np
import random

def gen_list_random_int(int_binf=0, intbsup=10, int_nombre=10)-> list:
    compteur = 0
    list_random = []
    while compteur != int_nombre:
        chiffre = random.randint(int_binf, intbsup-1)
        list_random.append(chiffre)
        compteur += 1
    return list_random

def nb_inf(L):
    nb_petit = 0
    for i in range(len(L)):
        if L[i] <= L[nb_petit]:
            nb_petit = i
    return nb_petit

def list_sorted(liste:list)->list:
    liste_fonction = copy.copy(liste)
    liste_ranger = []
    while len(liste_fonction) != 0:
        nombre = nb_inf(liste_fonction)
        liste_ranger.append(liste_fonction[nombre])
        del liste_fonction[nombre]
    return liste_ranger

liste_test = [5,10,6,7,2,7]
print(liste_test)

def test_temps(fonction:callable, config:int):
    liste_len = [10,100,1000]
    liste_elapsed = []
    for i in liste_len:
        temps = 0
        if config==1:
            liste = gen_list_random_int(0,i,i)
        elif config==2:
            liste = [i for i in range(i)]
        elif config ==3:
            liste = [i for i in range(i,-1,-1)]
        for j in range(100):
            start_pc = time.perf_counter()
            fonction(liste)
            end_pc = time.perf_counter()

            temps += end_pc - start_pc
        liste_elapsed.append(temps / 100)
    
    return liste_elapsed



test_1 = test_temps(list_sorted, 1)
test_2 = test_temps(sorted, 1)

fig, ax = plt.subplots()
ax.plot([10,100,1000],test_1,'bo-',label='Identité')
ax.plot([10,100,1000],test_2, 'r*-', label='Carré')
ax.set(xlabel='Abscisse x', ylabel='Ordonnée y',title='Fonctions identité, cube et carré')
ax.legend(loc='upper center', shadow=True, fontsize='x-large')
plt.show()