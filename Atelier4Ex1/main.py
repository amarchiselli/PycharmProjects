import random


def gen_list_random_int(int_binf=0, intbsup=10, int_nombre=10)-> list:
    compteur = 0
    list_random = []
    while compteur != int_nombre:
        chiffre = random.randint(int_binf, intbsup-1)
        list_random.append(chiffre)
        compteur += 1
    return list_random


print(gen_list_random_int(20, 40))
print(gen_list_random_int())