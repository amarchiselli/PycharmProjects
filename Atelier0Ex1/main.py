import math

def poidautorise(liste):
    poid = int(input("Indiquez le poid : "))
    while poid<0 or poid>liste[len(liste)-1]:
        poid = int(input("Indiquez le poid : "))
    return poid

def prixnet(poid, listepoid, listeprix):
    tarif = 0
    compteur = 0
    if 0 < poid <= listepoid[compteur]:
        tarif = listeprix[compteur]
    while tarif not in listeprix:
        compteur += 1
        if listepoid[compteur-1] < poid <= listepoid[compteur]:
            tarif = listeprix[compteur]
    return tarif

poidliste = [20,100,250,500,1000,3000]
prixliste = [1.16, 2.32, 4.00, 6.00, 7.50, 10.50]



poid = poidautorise(poidliste)
tarifnet = prixnet(poid, poidliste, prixliste)
print(tarifnet)