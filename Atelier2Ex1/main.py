def somme_v1(L):
    somme = 0
    for i in range(len(L)):
        somme = somme + L[i]
    return somme

def somme_v2(L):
    somme = 0
    for i in L:
        somme = somme + i
    return somme

def somme_v3(L):
    somme = 0
    compteur = 0
    while compteur != len(L):
        somme = somme + L[compteur]
        compteur += 1
    return somme

def test_exercice1():
    print("TEST SOMME")
    #test liste vide
    print("Test liste vide : ", moyenne([]))
    #test somme 11111
    S=[1,10,100, 1000,10000]
    print("Test somme 11111 : ", moyenne(S))

def moyenne(L):
    if len(L) != 0:
        resultat = somme_v1(L) / len(L)
    else:
        resultat = 0
    return resultat

def nb_sup_v1(L, e):
    nb_valeur_superieur_e = []
    for i in L:
        if L[i] >= e:
            nb_valeur_superieur_e.append(L[i])
    return nb_valeur_superieur_e

def nb_sup_v2(L, e):
    nb_valeur_superieur_e = []
    for i in L:
        if i >= e:
            nb_valeur_superieur_e.append(i)
    return nb_valeur_superieur_e

def moy_sup(L, e):
    nb_sup_L = nb_sup_v2(L, e)
    if nb_sup_L != 0:
        resultat = somme_v1(nb_sup_L) / len(nb_sup_L)
    else:
        resultat = 0
    return resultat

def val_max(L):
    valeur_max = L[0]
    for i in L:
        if valeur_max < i:
            valeur_max = i
    return valeur_max

def ind_max(L):
    valeur_max = val_max(L)
    for i in range(len(L)):
        if valeur_max == L[i]:
            indice = i
    return indice

liste_test=[1, 10, 10000, 100, 1000]

print(nb_sup_v2(liste_test, 1000),moy_sup(liste_test, 1000), moyenne(liste_test), val_max(liste_test), ind_max(liste_test))