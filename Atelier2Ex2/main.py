def position_for(L, e):
    position = -1
    for i in range(len(L)):
        if e == L[i]:
            position = i
    return position

def position_while(L, e):
    position = -1
    compteur = 0
    while position == -1 and compteur != len(L):
        if e == L[compteur]:
            position = compteur
        compteur += 1
    return position

def nb_occurrences(L, e):
    compteur = 0
    for i in range(len(L)):
        if e == L[i]:
            compteur +=1
    return compteur

def est_triee_for(L):
    bool = True
    for i in range(len(L)-1):
        if L[i] >= L[i+1]:
            bool = False
    return bool

def est_triee_while(L):
    bool = True
    compteur = 0
    while bool and compteur != len(L)-1:
        if L[compteur] >= L[compteur+1]:
            bool = False
        compteur += 1
    return bool

def position_tri(L, e):
    if est_triee_while(L) == False:
        liste_trier = sorted(L)
    else:
        liste_trier = L

    min = 0
    max = len(L) - 1
    milieu = 0

    while min <= max:
        milieu = (max + min) // 2
        if liste_trier[milieu] < e:
            min = milieu + 1
        elif liste_trier[milieu] > e:
            max = milieu - 1
        else:
            return liste_trier
    return -1

def a_repitition(L):
    bool = True
    compteur = 0
    liste_verif_repetition = []
    while bool and compteur != len(L):
        if L[compteur] not in liste_verif_repetition:
            liste_verif_repetition.append(L[compteur])
        else:
            bool = False
        compteur += 1
    return bool


liste_test = [10,100,1000,10000,1000]
element_test = 100

print(position_tri(liste_test, element_test), a_repitition(liste_test))