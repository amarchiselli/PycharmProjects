import random

# Fonction qui définie si une année est bissextile ou non
def est_bissextile(nbannee):
    if (nbannee%4 == 0 and nbannee%100 != 0) or nbannee%400 == 0:
        message = True
    else:
        message = False
    return message

# Fonction qui test la fonction 'est_bissextile'
def test_est_bissextile():
    compteur = 0
    while compteur < 5:
        compteur += 1
        nombrealeatoire = random.randint(1, 2000)
        print("Pour", nombrealeatoire, est_bissextile(nombrealeatoire))
    print("Pour 100", est_bissextile(100))
    return "Test terminé"

print(test_est_bissextile())