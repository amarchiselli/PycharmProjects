import math

# Fonction calculant le discriminant
def discriminant(a,b,c):
    resultat = (b**2)-(4*a*c)
    return resultat

# Fonction calculant la racine unique
def racine_unique(a, b):
    resultat = (-(b))/2*a
    return resultat

# Fonction calculant les racines double
def racine_double(a, b, delta, num):
    if num == 1:
        resultat = ((-(b)) + math.sqrt(delta)) / 2 * a
    else:
        resultat = ((-(b)) - math.sqrt(delta)) / 2 * a
    return resultat

# Fonction affichant  l'equation
def str_equation(a, b, c):
    if a > 0 or a < 0:
        resultat = "{}x2".format(a)
    else:
        resultat = ""
    if b > 0:
        resultat = resultat + "+{}x".format(b)
    elif b < 0:
        resultat = resultat + "{}x".format(b)
    if c > 0:
        resultat = resultat + "+{}".format(c)
    elif c < 0:
        resultat = resultat + "{}".format(c)
    resultat = resultat.replace("1x", "x")
    resultat = resultat + "=0"
    return resultat

# Fonction resolvant l'équation selon les valeurs implémenté sur a, b et c
def solution_equation(a, b, c):
    if a != 0 :
        nombre_discriminant = discriminant(a, b, c)
        if nombre_discriminant < 0:
            message = "Solution de l\'équation " + str_equation(a, b, c) + " Pas de racine réelle"
        elif nombre_discriminant == 0:
            message = "Solution de l\'équation " + str_equation(a, b, c) + " Racine unique : x=", racine_unique(a, b)
        elif nombre_discriminant > 0:
            message = "Solution de l\'équation " + str_equation(a, b, c) + " Deux racines: x1=", racine_double(a, b, nombre_discriminant, 1), "x2=", racine_double(a, b, nombre_discriminant, 2)
    if a == 0:
        message = "ERREUR EQUATION IMPOSSIBLE"
    return message

# Fonction implémentant les valeurs selectionné par l'utilisateur
def equation():
    A = int(input("Sélectionnez un chiffre pour a : "))
    while A == 0:
        print("ERREUR : Vous ne pouvez pas selectionner 0 pour a")
        A = int(input("Sélectionnez un chiffre pour a : "))
    B = int(input("Sélectionnez un chiffre pour b : "))
    C = int(input("Sélectionnez un chiffre pour c : "))
    return solution_equation(A, B, C)

# Fonction testant le programme
def test_equation():
    print(solution_equation(10,3,5))
    print(solution_equation(1,2,1))
    print(solution_equation(-3,8,-2))
    print(solution_equation(0, 1, 1))


test_equation()
print(equation())