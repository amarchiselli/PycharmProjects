import copy

def est_triee_while(L):
    bool = True
    compteur = 0
    while bool and compteur != len(L)-1:
        if L[compteur] >= L[compteur+1]:
            bool = False
        compteur += 1
    return bool


def non_doublon(element, L2):
    bool = True
    if element in L2:
        bool = False
    return bool


def modificateur_liste(liste_complete, liste, compteur):
    liste_test = copy.copy(liste_complete[compteur])
    element = 0
    element_liste_complete = 0
    while len(liste_test) != 0:
        if liste_test[element_liste_complete] == liste[element]:
            del liste[element]
            liste_test.pop(element_liste_complete)
            element = 0
        else:
            element += 1
    return liste

def creation_mini_liste(nb_emplacement, liste):
    compteur = 0
    element = 0
    liste_vitrine = []
    liste_doublon = []
    while compteur != nb_emplacement and element != len(liste):
        if not non_doublon(liste[element], liste_doublon):
            liste_vitrine.append(liste[element])
            compteur += 1
            element += 1
        else:
            liste_doublon.append(liste[element])
            element += 1
    element = 0
    while compteur != nb_emplacement and element != len(liste):
        if non_doublon(liste[element], liste_vitrine):
            liste_vitrine.append(liste[element])
            compteur += 1
            element += 1
        else:
            element += 1
    return liste_vitrine

def creation_vitrine(nb_emplacement, liste):
    liste_complete = []
    compteur = 0
    if est_triee_while(liste) == False:
        liste_trier = sorted(liste)
    else:
        liste_trier = liste
    while compteur != 2:
        if compteur == 0:
            liste_complete.append(creation_mini_liste(nb_emplacement, liste_trier))
        else:
            liste_complete.append(creation_mini_liste(nb_emplacement, liste_modifie))
        liste_modifie = modificateur_liste(liste_complete, liste_trier, compteur)
        compteur += 1
    return liste_complete

nb_emplacement = 4

liste_objet = [1,3,4,0,2,2,5,5]
liste_objet2 = [1,3,4,0,2,2,5,5]

print(creation_vitrine(nb_emplacement, liste_objet))