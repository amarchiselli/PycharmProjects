def separer(L):
    liste_negative = []
    liste_nul = []
    liste_positive = []

    for i in range(len(L)):
        if L[i] < 0:
            liste_negative.append(L[i])
        elif L[i] == 0:
            liste_nul.append(L[i])
        elif L[i] > 0:
            liste_positive.append(L[i])

    return liste_negative + liste_nul + liste_positive

liste_test = [0,-1,5,3,-7,0,-2]

print(separer(liste_test))