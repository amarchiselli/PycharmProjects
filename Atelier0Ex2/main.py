import math

# Fonction permettant la selection du type de lettre
def selection_du_type(liste):
    type = str.lower(input("Choisissez le type de lettre utilisée (verte, prioritaire, éco-pli) : "))
    while type not in liste:
        type = str.lower(input("Choisissez le type de lettre utilisée (verte, prioritaire, éco-pli) : "))
    return type

# Fonction permettant la selection du poid
def selection_du_poid(liste):
    poid = int(input("Indiquez le poid : "))
    while poid < 0 or poid > liste[len(liste)-1]:
        poid = int(input("Indiquez le poid : "))
    return poid

# Fonction calculant la tranche du poid si nécessaire
def tranche_poid(poid):
    if poid > 100:
        resultat = math.ceil(poid / 10)
    else:
        resultat = 0
    return resultat

# Cette fonction donne le tarif net selon le poid de la lettre
def tarif_net(poid, liste_poid, liste_prix):
    tarif = 0
    compteur = 0
    if 0 < poid <= liste_poid[compteur]:
        tarif = liste_prix[compteur]
    while tarif not in liste_prix:
        compteur += 1
        if liste_poid[compteur - 1] < poid <= liste_poid[compteur]:
            tarif = liste_prix[compteur]
    return tarif

# Cette fonction donne le complément selon le type de la lettre
def complement_transport(chiffre, type_lettre, liste):
    if chiffre == 1 and type_lettre == liste[2]:
        complement = 0.02
    if chiffre == 1 and (type_lettre == liste[0] or type_lettre == liste[1]) or chiffre == 2 and type_lettre == liste[2]:
        complement = 0.05
    if chiffre == 2 and (type_lettre == liste[0] or type_lettre == liste[1]):
        complement = 0.11
    return complement

# Cette fonction crée deux liste selon où se trouve la variable type dans la liste
def implementation_des_listes(type, liste):
    if type == liste[0]:
        poid_liste = [20, 100, 250, 500, 1000, 3000]
        prix_liste = [1.16, 2.32, 4.00, 6.00, 7.50, 10.50]
    if type == liste[1]:
        poid_liste = [20, 100, 250, 500, 3000]
        prix_liste = [1.43, 2.86, 5.26, 7.89, 11.44]
    if type == liste[2]:
        poid_liste = [20, 100, 250]
        prix_liste = [1.14, 2.28, 3.92]
    liste_complete = [poid_liste, prix_liste]
    return liste_complete

# Cette fonction indique dans quel région en outre-mer se trouve la région selectionné
def complement_selon_zone(zone, liste_1, liste_2):
    if zone in liste_1:
        resultat = complement_transport(1, type_lettre, type_lettre_liste)
    if zone in liste_2:
        resultat = complement_transport(2, type_lettre, type_lettre_liste)
    return resultat

# Contient la liste des différent type sélectionnable
type_lettre_liste = ["verte", "prioritaire", "éco-pli"]

# Demande à l'utilisateur le type de lettre qu'il souhaite envoyer
type_lettre = selection_du_type(type_lettre_liste)

# Créer deux listes selon le type de la lettre a envoyer
# 'liste_poid_et_prix[0]' contient la liste des poids et 'liste_poid_et_prix[1]' contient les tarifs net associé à ces poids
liste_poid_et_prix = implementation_des_listes(type_lettre, type_lettre_liste)

# Demande à l'utilisateur le poid de la ou les lettres
poid = selection_du_poid(liste_poid_et_prix[0])

# Contient les listes des zones en outre-mer
zone_outre_mer_1 = ["guyane", "guadeloupe", "martinique", "laréunion", "st pierre et miquelon", "st barthélémy", "st-martin et mayotte"]
zone_outre_mer_2 = ["nouvelle-calédonie", "polynésie française", "wallis-et-futuna", "t.a.a.f."]

# Demande à l'utilisateur dans quel région il se trouve
zone_selectionne = str.lower(input("Sélectionnez votre région en outre-mer: "))
while zone_selectionne not in zone_outre_mer_1 and zone_selectionne not in zone_outre_mer_2:
    zone_selectionne = str.lower(input("Sélectionnez votre région en outre-mer: "))

# Calcule le tarif total
tarif = tarif_net(poid, liste_poid_et_prix[0], liste_poid_et_prix[1]) + tranche_poid(poid) * complement_selon_zone(zone_selectionne, zone_outre_mer_1, zone_outre_mer_2)

# Rajoute 50 centimes si la personne veut un timbre
timbre = str.upper(input("Voulez-vous mettre un timbre (O/N)? "))
if timbre == "O":
    tarif = tarif + 0.50

print(tarif,"€")