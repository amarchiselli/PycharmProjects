import re

def full_name(str_arg:str)->str:
    liste_string = str_arg.split(" ")
    str_arg1 = liste_string[0].upper()
    str_arg2 = liste_string[1].capitalize()
    return str_arg1 + " " + str_arg2

def verif_corps(string):
    liste = re
    erreur = False
    for i in string:
        if i in liste:
            erreur = True
    return erreur

def is_mail(str_arg:str)->tuple:
    erreur = False
    while erreur == False:
        if '@' not in str_arg:
            tuple_erreur = (0, 2)
            erreur = True
        separe = str_arg.split('@')
        if len(separe) == 2:
            if separe[0] == "":
                tuple_erreur = (0, 1)
                erreur = True
            elif separe[0][0] == "." or separe[0][-1] == ".":
                tuple_erreur = (0, 1)
                erreur = True
            elif ".." in separe[0]:
                tuple_erreur = (0, 1)
                erreur = True
            elif not re.match("^[a-zA-Z0-9()$%_\-.]*$", separe[0]):
                tuple_erreur = (0, 1)
                erreur = True
            elif separe[1][0:10] != "univ-corse":
                tuple_erreur = (0, 3)
                erreur = True
            elif separe[1][10] != ".":
                tuple_erreur = (0, 4)
                erreur = True
            else:
                tuple_erreur = (1, 0)
                erreur = True
    return tuple_erreur

str_variable = "marchiselli.Anthony@univ-corse.fr"

print(is_mail(str_variable))