import random

# Cette fonction prend les postions d'une lettre dans un mot
def places_lettre(ch:str, mot:str)->list:
    list_position = []
    for i in range(len(mot)):
        if ch == mot[i]:
            list_position.append(i)
    return list_position

# Cette fonction afiche le mot selon la position des lettres découvertes
def affichage_mot(mot:str, lpos:list)-> str:
    mot_pendu = ""
    for i in range(len(mot)):
        if i in lpos:
            mot_pendu += mot[i]
        elif mot[i] == " ":
            mot_pendu += mot[i]
        elif mot[i] == "-":
            mot_pendu += mot[i]
        else:
            mot_pendu += "_"
    return mot_pendu

# Cette fonction construit le pendu selon le nombre d'erreurs
def constructeur_pendu(compteur):
    if compteur == 5:
        print("|---] ")
    if compteur >= 4:
        print("|   O ")
    if compteur >= 3:
        print("|   T ")
    if compteur >= 2:
        print("|  / \ ")
    if compteur >= 1:
        print("|______")

# Cette fonction lance le jeu
def run_game():
    vouloir_jouer = True
    while vouloir_jouer: # Boucle qui permet au joueur de rejouer
        liste = build_liste("capitales_pays.txt") # Ramène la liste des pays
        mot = build_dict(liste) # Renvoi un mot dans la liste
        compteur = 0 # Une variable pour compter le nombre d'erreurs
        victoire = False # un booléen pour vérifier si le joueur a gagné
        list_lettre_trouve = [] # Comporte la liste des positions des lettres trouvées
        liste_choix = [] # Comporte la liste des lettres selectionnées
        while compteur != 5 and not victoire: # Boucle qui lance le jeu jusqu'à que le joueur gagne ou perde
            choix = input("Choisissez une lettre : ").lower() # demande au joueur de choisir une lettre et la transforme en minuscule si ce n'est pas le cas
            while choix in liste_choix or len(choix) != 1: # Si le joueur choisit une lettre qu'il a déjà choisit avant ou il met plus d'une lettre ou il met un chiffre alors cette boucle redemande jusqu'à qu'il choisissent une option possible
                print("Veuillez choisir une lettre qui n'a pas encore été essayé")
                choix = input("Choisissez une lettre : ").lower()
            liste_choix.append(choix) # Rajoute le choix dans la liste des lettres déjà selectionnées
            lettre_trouve = places_lettre(choix, mot) # Indique la/les positions de la lettre selectionnées dans le mot
            if lettre_trouve == []: # Verifie si la lettre n'a aucune position dans le mot
                compteur += 1 # Augmente le compteur si c'est le cas
                print("Il n'y a pas de " + choix) # Indique au joueur que la lettre qu'il a choisit n'est pas dans le mot
            else:
                for i in lettre_trouve: # Une boucle qui verifie si l'élément ou les elements sont deja present dans la liste puis les ajoutes dans la liste complète
                    if i not in list_lettre_trouve:
                        list_lettre_trouve.append(i)
                print("Il y a un ou plusieurs " + choix) # Indique au joueur que la lettre qu'il a choisit est dans le mot
            constructeur_pendu(compteur) # Construit le pendu selon le nombre d'erreurs
            print(affichage_mot(mot, list_lettre_trouve)) # Affiche le mot selon les lettres trouvées
            print(liste_choix) # Affiche les choix déjà prit
            if affichage_mot(mot, list_lettre_trouve) == mot: # Vérifie si le mot est trouvé
                victoire = True # Passe victoire à True pour sortir de la boucle
        if victoire: # Retourne un message si le joueur a gagné
            print("Bravo, vous avez trouvé le mot")
        else: # Retourne un message si le joueur a perdu
            print("Dommage, peut-être une prochaine fois")
        reesayer = input("Voulez-vous jouer à nouveau (O/N)? ").upper() # Demande si le joueur veut rejouer
        if reesayer == "N":
            vouloir_jouer = False # Si le joueur met N alors il sort de la boucle vouloir_joueur
    return "Jeu terminé"

# Cette fonction crée une liste en prenant les éléments dans un fichier
def build_liste(fichier:str)->list:
    f = open(fichier, "r", encoding="utf-8")
    c = f.readline()
    list_mot = []
    # lecture d'une ligne dans une chaine de caractères
    while c != "":
        c_modifie = c.replace("\n", "")
        c_modifie = c_modifie.replace("\t", "")
        list_mot.append(c_modifie.lower())
        c = f.readline()
    return list_mot

# Cette fonction crée un dictionnaire ayant pour clé la taille des mots
def build_dict(lst:list)->str:
    dictionnaire = dict()
    for i in range(30): # Boucle qui ajoute toute les clées possible dans l'ordre
        dictionnaire[i] = ""
    for i in range(len(lst)): # Boucle pour construire entièrement le dictionnaire
        objet = lst[i].replace(" ", "")
        objet = objet.replace("-", "")
        if dictionnaire[len(objet)] != "": # Vérifie si la clé est déjà assignée à une liste
            liste = []
            for j in dictionnaire[(len(objet))]: # Ajoute la liste associée à la clé dans une autre liste
                liste.append(j)
            liste.append(lst[i]) # Ajoute le nouvel élément dans la liste
            dictionnaire[len(objet)] = liste # Change la liste associé à une clé par la liste modifié
        else: # Se lance si la clé n'a pas de valeur assignée et crée une liste
            dictionnaire[len(objet)] = [lst[i]]
    for i in range(30): # Si la clé n'est associée à aucune valeur, elle est supprimée
        if dictionnaire[i] == "":
            del dictionnaire[i]

    difficulte_choisit = input("Choisissez une difficulté (facile, intermediaire, difficile): ") # Demande quel difficulté veux le joueur
    while difficulte_choisit != "facile" and difficulte_choisit != "intermediaire" and difficulte_choisit !=  "difficile":
        difficulte_choisit = input("Choisissez une difficulté (facile, intermediaire, difficile): ")

    mot = choix_difficulte(dictionnaire, difficulte_choisit)

    return mot

# Cette fonction renvoi un mot aléatoire selon la difficulté sélectionnée
def choix_difficulte(sorted_words:dict, selection_difficulte:str)->str:
    difficulte = []
    if selection_difficulte == "facile":
        taille_mot = 0, 7
    elif selection_difficulte == "intermediaire":
        taille_mot = 7, 9
    else:
        taille_mot = 9, 30
    for i in range(taille_mot[0],taille_mot[1]):
        if i in sorted_words:
            difficulte.append(i)
    taille_aleatoire = random.choice(difficulte)
    mot = random.choice(sorted_words[taille_aleatoire])

    return mot


print(run_game())