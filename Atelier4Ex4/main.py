import random

def extract_element_list(liste, chiffre):
    compteur = 0
    liste_elements = []
    while compteur != chiffre:
        element = random.randint(0, len(liste) - 1)
        if element not in liste_elements:
            liste_elements.append(liste[element])
            compteur +=1
    return liste_elements

liste_range = [i for i in range(10)]
print(extract_element_list(liste_range, 3))