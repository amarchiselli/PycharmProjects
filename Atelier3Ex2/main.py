def mots_n_lettres(lst_mot, n):
    list_n = []
    for i in lst_mot:
        if len(i) == n:
            list_n.append(i)
    return list_n

def commence_par(mot, prefixe):
    taille_prefixe = len(prefixe)
    return prefixe == mot[:taille_prefixe]

def finit_par(mot, suffixe):
    taille_suffixe = len(suffixe)
    return suffixe == mot[-taille_suffixe:]

def finissent_par(lst_mot, suffixe):
    lst_suffixe =[]
    for i in range(len(lst_mot)):
        bool = finit_par(lst_mot[i], suffixe)
        if bool:
            lst_suffixe.append(lst_mot[i])
    return lst_suffixe

def commencent_par(lst_mot, prefixe):
    lst_prefixe =[]
    for i in range(len(lst_mot)):
        bool = commence_par(lst_mot[i], prefixe)
        if bool:
            lst_prefixe.append(lst_mot[i])
    return lst_prefixe

def liste_mots(lst_mot, prefixe, suffixe, n):
    lst_prefixe = commencent_par(lst_mot, prefixe)
    lst_ranger = finissent_par(lst_prefixe, suffixe)
    return mots_n_lettres(lst_ranger, n)

def dictionnaire(fichier):
    f = open(fichier, "r", encoding = "utf-8")
    c = f.readline()
    # lecture d'une ligne dans une chaine de caractères
    print("** Contenu du fichier **")
    while c!="":
        print(c)
        c=f.readline()
    print("** fin **")

def test_exercice(nombre_test):
    lst_mot = ["jouer","bonjour", "punir", "jour", "aurevoir", "revoir", "pouvoir", "cour", "abajour", "finir", "aimer"]
    for i in range(nombre_test + 1):
        liste = mots_n_lettres(lst_mot, i)
        print(liste, i)
    return "Test Effectué"

lst_mot = ["jouer","bonjour", "punir", "jour", "aurevoir", "revoir", "pouvoir", "cour", "abajour", "finir", "aimer"]

print(liste_mots(lst_mot, "jou", "r", 5))
print(dictionnaire("littre.txt"))