#import matplotlib.pyplot as plt

def valeur_max(F):
    valeur_max = F[0]
    for i in F:
        if valeur_max < i:
            valeur_max = i
    return valeur_max

def histo(F):
    valeur_maximum = valeur_max(F)

    H = []
    compteur_H = 0
    while len(H) != valeur_maximum + 1:
        compteur = 0
        for i in range(len(F)):
            if compteur_H == F[i]:
                compteur += 1
        H.append(compteur)
        compteur_H +=1
    return H

def est_injective(F):
    bool = True
    H = histo(F)
    for i in range(len(H)):
        if H[i] > 1:
            bool = False
    return bool

def est_surjective(F):
    bool = True
    H = histo(F)
    for i in range(len(H)):
        if H[i] < 1:
            bool = False
    return bool

def est_bijective(F):
    bool = False
    if est_injective(F) and est_surjective(F):
        bool = True
    return bool

def affiche_histo(F):
    H = histo(F)
    valeur_maximum = valeur_max(H)

    print('HISTOGRAMME')
    for i in range(valeur_maximum):
        for j in range(len(H)):
            if H[j] >= valeur_maximum-i:
                print('   #', end='')
            else:
                print('   |', end='')
        print()
    for k in range(len(H)):
        if k<10:
            print('   ' + str(k), end='')
        else:
            print('  ' + str(k), end='')
    print()
    return "Test Terminé\n"


test = [0,5,5,5,6,7,2,3,1,6,2,4]

print(affiche_histo(test), est_bijective(test), est_injective(test), est_surjective(test))