import random


def mix_list(liste:list)->list:
    liste_mixe = []
    liste_modifie = liste
    for i in range(len(liste)):
        nb_random = random.choice(liste_modifie)
        liste_mixe.append(nb_random)
        liste_modifie.remove(nb_random)
    return liste_mixe

liste_chiffre = [i for i in range(10)]
print(mix_list(liste_chiffre))