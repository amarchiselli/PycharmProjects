def image_imc(imc, listeInterprétation, listeRepere):
    compteur = 0
    compteurmax = len(listeRepere)-1
    compteurmaxinterpretation = len(listeInterprétation)-1
    message = ""
    if imc <= listeRepere[compteur]:
        message = listeInterprétation[compteur]
    if imc <= listeRepere[compteurmax]:
        while message not in listeInterprétation:
            compteur += 1
            if listeRepere[compteur-1] < imc <= listeRepere[compteur]:
                message = listeInterprétation[compteur]
    if imc > listeRepere[compteurmax]:
        message = listeInterprétation[compteurmaxinterpretation]
    return message

def test_image_imc():
    x = 0
    while x < 50:
        x += 1
        print("Pour un IMC de", x, "l'individu a un poid",image_imc(x, listeInterprétation, repereImc))

repereImc = [16.5,18.5,25,30,35,40]
listeInterprétation = ["dénutrition","maigreur","corpulence normale","surpoids","obésité modérée","obésité sévère","obésité sévère"]

print(test_image_imc())